from rest_framework import serializers
from ..models.photo import Photo
from ..serializers.order_serializer import OrderSerializer


class PhotoSerializer(serializers.ModelSerializer):
    order = OrderSerializer(read_only=True)

    class Meta:
        model = Photo
        fields = (
            'id', 'path', 'width', 'height', 'filename',
            'likes', 'location', 'scale',
            'username', 'date', 'order', 'user_picture',
            'rotated'
        )
