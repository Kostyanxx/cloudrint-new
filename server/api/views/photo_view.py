from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from ..models.order import Order
from ..models.photo import Photo
from ..serializers.photo_serializer import PhotoSerializer
from django.http import JsonResponse
from django.db.models import Q
import os
import secrets
from PIL import Image, ImageDraw
import base64
from django.core.paginator import Paginator


def save_photo(path, filename, uploaded_file, is_instagram, order, photo):
    full_path = os.path.join(path, filename)
    width = 640
    height = 480

    if not os.path.exists(path):
        os.makedirs(path)

    with open(full_path, 'wb+') as destination:
        if is_instagram:
            destination.write(base64.b64decode(uploaded_file))
        else:
            for chunk in uploaded_file.chunks():
                destination.write(chunk)

    im = Image.open(full_path)
    i_width, i_height = im.size

    if i_width>i_height:
        im = im.rotate(90, expand=1)
        photo["rotated"] = True
    if not os.path.exists(path + '/previews/'):
        os.makedirs(path + '/previews/')

    if not os.path.exists(path + '/original/'):
        os.makedirs(path + '/original/')

    rgb_im = im.convert('RGB')
    rgb_im1 = im.convert('RGB')

    if i_width > i_height:
        rgb_im.thumbnail((height / 2, i_width), Image.ANTIALIAS)
    else:
        rgb_im.thumbnail((width / 2, i_height), Image.ANTIALIAS)

    rgb_im.save(path + '/previews/' + filename, "JPEG")
    imgfile = open(path + '/previews/' + filename, 'rb').read()
    b64img = str(base64.b64encode(imgfile), 'utf-8')
    b64imgfile = open(path + '/previews/' + filename, 'w')

    for line in b64img:
        b64imgfile.write(line)

    if i_width > i_height:
        rgb_im1.thumbnail((height, i_width), Image.ANTIALIAS)
    else:
        rgb_im1.thumbnail((width, i_height), Image.ANTIALIAS)

    rgb_im1.save(path + '/original/' + filename, "JPEG")
    imgfile = open(path + '/original/' + filename, 'rb').read()
    b64img = str(base64.b64encode(imgfile), 'utf-8')
    b64imgfile = open(path + '/original/' + filename, 'w')

    for line in b64img:
        b64imgfile.write(line)

    serializer = PhotoSerializer(data=photo)

    if serializer.is_valid(raise_exception=True):
        photo_save = serializer.save(order=order)
        return photo_save

    return False


def add_corners(im, rad):
    circle = Image.new('L', (rad * 2, rad * 2), 0)
    draw = ImageDraw.Draw(circle)
    draw.ellipse((0, 0, rad * 2, rad * 2), fill=255)
    alpha = Image.new('L', im.size, 255)
    w, h = im.size
    alpha.paste(circle.crop((0, 0, rad, rad)), (0, 0))
    alpha.paste(circle.crop((0, rad, rad, rad * 2)), (0, h - rad))
    alpha.paste(circle.crop((rad, 0, rad * 2, rad)), (w - rad, 0))
    alpha.paste(circle.crop((rad, rad, rad * 2, rad * 2)), (w - rad, h - rad))
    im.putalpha(alpha)
    return im


def save_instagram_user_photo(path, content):
    full_path = os.path.join(path, 'user.jpg')

    if not os.path.exists(path):
        os.makedirs(path)

    with open(full_path, 'wb+') as destination:
        destination.write(base64.b64decode(content))

    im = Image.open(full_path)
    rgb_im = add_corners(im, 80)
    rgb_im.save(path + '/user.png')


class PhotoView(viewsets.ViewSet):
    def list(self, request, pk=None):
        photos = Photo.objects.filter(
            Q(order_id=pk, order__client_id=request.client)
        )
        photos_count = photos.count()
        paginator = Paginator(photos, 5)
        photos = paginator.get_page(request.GET["page"])
        has_next = photos.has_next()

        serializer = PhotoSerializer(photos, many=True)
        for photo in serializer.data:
            with open(photo['path'] + '/previews/' + photo['filename'], "rb") as image_file:
                photo['content'] = image_file.read()
                photo['image'] = photo['order']['image']

            del photo['path']
            del photo['filename']
            del photo['order']

        return Response({
            "photos": serializer.data,
            "count": photos_count,
            "has_next": has_next
        })

    def update(self, request, pk=None):
        photo = request.data
        queryset = Photo.objects.all()
        photo_saved = get_object_or_404(
            queryset, pk=pk, order__client_id=request.client
        )
        photo_saved.scale = photo["scale"]
        photo_saved.save()

        return JsonResponse({
            "photo_id": photo_saved.id
        })

    def delete(self, request, pk=None):
        queryset = Photo.objects.all()
        photo = get_object_or_404(
            queryset, pk=pk, order__client_id=request.client
        )
        photo.delete()
        return JsonResponse({
            "success": True
        })

    def upload(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(
            queryset, pk=pk, client_id=request.client
        )
        try:
            uploaded_file = request.FILES['file']
            filename = secrets.token_hex(15)
            path = 'uploads/' + str(order.id)
            photo = {
                "path": path,
                "filename": filename,
            }

            photo_save = save_photo(path, filename, uploaded_file, False, order, photo)

            if photo_save:
                return JsonResponse({
                    "photo_id": photo_save.id
                })

            return JsonResponse({
                "error photo": True
            })
        except:
            return JsonResponse({
                "error": "unable to save photo"
            })

    def create(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(
            queryset, pk=pk, client_id=request.client
        )

        file_content = request.data.get('content')
        filename = secrets.token_hex(15)
        path = 'uploads/' + str(order.id)
        photo = {
            "path": path,
            "filename": filename,
            "username": request.data.get('username'),
            # "user_picture": "empty_path",
            "likes": request.data.get('likes'),
            "date": request.data.get('date'),
            "location": request.data.get('location')
        }

        photo_save = save_photo(path, filename, file_content, True, order, photo)

        if photo_save:
            return JsonResponse({
                "photo_id": photo_save.id
            })

        return JsonResponse({
            "error photo": True
        })


    def create_instagram_user_photo(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(
            queryset, pk=pk, client_id=request.client
        )
        try:
            file_content = request.data.get('content')
            path = 'uploads/' + str(order.id)
            save_instagram_user_photo(path, file_content)
            return JsonResponse({
                "success": True
            })
        except:
            return JsonResponse({
                "error": "unable to create instagram user photo"
            })

    def get_instagram_user_photo(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(
            queryset, pk=pk, client_id=request.client
        )
        try:
            path = 'uploads/' + str(order.id) + '/user.png'
            image = None

            with open(path, "rb") as image_file:
                image = base64.b64encode(image_file.read())

            return JsonResponse({
                "content": str(image, 'utf-8')
            })
        except:
            return JsonResponse({
                "error": "unable to get instagram user photo"
            })
