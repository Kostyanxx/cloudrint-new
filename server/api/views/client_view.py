from rest_framework import viewsets
from django.http import JsonResponse
from ..models.client import Client
import secrets


class ClientView(viewsets.ViewSet):

    def check(self, request):
        return JsonResponse({
            "success": True
        })

    def auth(self, request):
        token = secrets.token_hex(20)
        ip = request.META.get('REMOTE_ADDR', 'none')
        user_agent = request.META.get('HTTP_USER_AGENT', 'none')
        Client.objects.get_or_create(ip=ip, token=token, user_agent=user_agent)
        return JsonResponse({"token": token})


