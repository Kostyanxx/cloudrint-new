from rest_framework import viewsets
from django.http import JsonResponse
import requests


class InstagramView(viewsets.ViewSet):
    def back_auth(self, request):
        return JsonResponse({
            "success": True
        })

    def clear_instagram_data(self, request):
        return JsonResponse({
            "success": True
        })

    def get_token(self, request):
        code = request.GET["code"]
        url = "https://api.instagram.com/oauth/access_token"
        payload = 'client_id=711679642940859&' \
                  'client_secret=3396cabe8e0b2885681280166023608f' \
                  '&grant_type=authorization_code' \
                  '&redirect_uri=https%3A//localhost%3A3000/instagram' \
                  '&code=' + code
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        return JsonResponse(response.json())
