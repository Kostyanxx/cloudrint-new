from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from ..models.order import Order
from ..models.client import Client
from ..models.photo import Photo
from ..serializers.order_serializer import OrderSerializer
from django.http import JsonResponse
from django.db.models import Q
import pika
from ..serializers.photo_serializer import PhotoSerializer
import json


class OrderView(viewsets.ViewSet):
    def list(self, request):
        orders = Order.objects.filter(
            Q(client_id=request.client)
        )
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)

    def details(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        serializer = OrderSerializer(order)
        response = serializer.data
        response['hasPhotos'] = Photo.objects.filter(
            Q(order_id=pk)
        ).exists()
        return Response(response)

    def create(self, request):
        order = request.data
        client = Client.objects.get(pk=request.client)
        serializer = OrderSerializer(data=order)
        if serializer.is_valid(raise_exception=True):
            order_save = serializer.save(client=client)
            return JsonResponse({
                "order_id": order_save.id
            })

    def update(self, request, pk=None):
        order = request.data
        serializer = OrderSerializer(data=order)
        if serializer.is_valid(raise_exception=True):
            queryset = Order.objects.all()
            order_saved = get_object_or_404(queryset, pk=pk)
            for key, value in serializer.validated_data.items():
                setattr(order_saved, key, value)
            order_saved.save()
            return JsonResponse({
                "order_id": order_saved.id
            })

    def delete(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        order.delete()
        return JsonResponse({
            "success": True
        })

    def generate(self, request, pk=None):
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        channel.queue_declare(queue='photo')

        photos = Photo.objects.filter(
            Q(order_id=pk)
        )
        serializer = PhotoSerializer(photos, many=True)
        for photo in serializer.data:
            photo['image'] = photo['order']['image']
            channel.basic_publish(exchange='',
                                  routing_key='photo',
                                  body=json.dumps(photo))
        connection.close()
        return Response(1)
