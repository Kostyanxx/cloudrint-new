from django.contrib import admin
from .models.client import Client
from .models.order import Order
from .models.photo import Photo


admin.site.register(Order)
admin.site.register(Client)
admin.site.register(Photo)
