from django.urls import path
from .views.client_view import ClientView
from .views.order_view import OrderView
from .views.photo_view import PhotoView
from .views.instagram_view import InstagramView

app_name = "api"

urlpatterns = [
    path('clients/auth', ClientView.as_view({
        'get': 'check',
        'post': 'auth',
    })),
    path('clients/orders', OrderView.as_view({
        'get': 'list',
        'post': 'create',
    })),
    path('clients/orders/<int:pk>', OrderView.as_view({
        'get': 'details',
        'patch': 'update',
        'delete': 'delete'
    })),
    path('clients/orders/<int:pk>/generate', OrderView.as_view({
        'post': 'generate',
    })),
    path('clients/orders/<int:pk>/photos', PhotoView.as_view({
        'get': 'list',
    })),
    path('clients/orders/<int:pk>/photo', PhotoView.as_view({
        'post': 'create',
        'put': 'upload'
    })),
    path('clients/orders/<int:pk>/photo/user', PhotoView.as_view({
        'get': 'get_instagram_user_photo',
        'post': 'create_instagram_user_photo'
    })),
    path('clients/orders/photo/<int:pk>', PhotoView.as_view({
        'patch': 'update',
        'delete': 'delete',
    })),
    path('clients/instagram', InstagramView.as_view({
        'get': 'get_token'
    })),
]