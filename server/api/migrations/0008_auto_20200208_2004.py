# Generated by Django 2.1.11 on 2020-02-08 20:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20191215_2326'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='type',
            field=models.IntegerField(choices=[(1, 'DEFAULT_TYPE'), (2, 'INSTAGRAM_TYPE')], default=1),
        ),
        migrations.AlterField(
            model_name='photo',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos', to='api.Order'),
        ),
    ]
