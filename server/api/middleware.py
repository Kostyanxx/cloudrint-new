from django.utils.deprecation import MiddlewareMixin
from .models.client import Client
from django.http import JsonResponse


class GetClient(MiddlewareMixin):

    def process_request(self, request):
        request.client = 0
        if request.path == "/api/clients/auth" and request.method == "POST":
            return None
        if request.path[0:12] != "/api/clients":
            return None

        auth_header = request.META.get('HTTP_AUTHORIZATION')

        if auth_header:
            auth_header = auth_header.split(' ')
            if len(auth_header) > 1:
                try:
                    client = Client.objects.get(token=auth_header[1])
                except:
                    return JsonResponse({'error': 'client not found'}, status=500)
                if client:
                    request.client = client.id
                return None

        return JsonResponse({'error': 'client not found'}, status=500)
