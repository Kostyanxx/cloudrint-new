from django.db import models


class Client(models.Model):
    token = models.CharField(max_length=100)
    ip = models.CharField(max_length=100)
    user_agent = models.TextField()

    def __str__(self):
        return self.token