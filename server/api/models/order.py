from django.db import models
from .client import Client


class Order(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='orders'
    )

    DEFAULT_TYPE = 1
    INSTAGRAM_TYPE = 2
    ORDER_TYPES = [
        (DEFAULT_TYPE, 'DEFAULT_TYPE'),
        (INSTAGRAM_TYPE, 'INSTAGRAM_TYPE'),
    ]
    type = models.IntegerField(
        choices=ORDER_TYPES,
        default=DEFAULT_TYPE
    )

    DEFAULT_IMAGE = 1
    FULL_IMAGE = 2
    ORDER_IMAGES = [
        (DEFAULT_IMAGE, 'DEFAULT_IMAGE'),
        (FULL_IMAGE, 'FULL_IMAGE'),
    ]
    image = models.IntegerField(
        choices=ORDER_IMAGES,
        default=DEFAULT_IMAGE,
    )

    PHOTO_10X15 = 1
    PHOTO_10X10 = 2
    PHOTO_SIZES = [
        (PHOTO_10X15, 'PHOTO_10X15'),
        (PHOTO_10X10, 'PHOTO_10X10'),
    ]
    size = models.IntegerField(
        choices=PHOTO_SIZES,
        default=PHOTO_10X15
    )

    DEFAULT_TEMPLATE = 1
    INSTAGRAM_TEMPLATE = 2
    ORDER_TEMPLATES = [
        (DEFAULT_TEMPLATE, 'DEFAULT_TEMPLATE'),
        (INSTAGRAM_TEMPLATE, 'INSTAGRAM_TEMPLATE'),
    ]
    template = models.IntegerField(
        choices=ORDER_TEMPLATES,
        default=DEFAULT_TEMPLATE
    )

    count = models.IntegerField()
