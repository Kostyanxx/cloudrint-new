from django.db import models
from .order import Order


class Photo(models.Model):
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='photos'
    )
    path = models.CharField(max_length=255)
    filename = models.CharField(max_length=255)
    user_picture = models.CharField(max_length=255, default=None, blank=True, null=True)
    width = models.IntegerField(default=None, blank=True, null=True)
    height = models.IntegerField(default=None, blank=True, null=True)
    likes = models.IntegerField(default=None, blank=True, null=True)
    location = models.CharField(max_length=255, default=None, blank=True, null=True)
    scale = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        default=None, blank=True, null=True
    )
    username = models.CharField(max_length=255, default=None, blank=True, null=True)
    date = models.IntegerField(default=None, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    rotated = models.BooleanField(default=False)
