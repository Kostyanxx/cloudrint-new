export declare type Orders = OrderType[]

export interface OrderType {
    id: number
    image: number
    type: number
    size: number
    template: number
    count: number
    hasPhotos: boolean
    [index: string]: any
}

export interface OrderValidateFields {
    image: boolean
    size: boolean
    template: boolean
    count: boolean
    type: boolean
}

export const OrderRequireFields: OrderValidateFields = {
    image: true,
    size: true,
    template: true,
    count: true,
    type: true
};