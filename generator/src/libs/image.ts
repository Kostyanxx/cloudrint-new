import {PhotoType} from "../types/PhotoType";
import {fabric} from "fabric";
import {getMonthName} from "./date";
import {OrderType} from "../types/OrderType";
import {
    OrderTemplate,
    OrderImage,
    OrderSize
} from '../enums/OrderEnum';

export interface PhotoSetting {
    url: string,
    settings?: any
}

const globalWidth = 960;
let globalHeight = 1280;
const scaleC = 4;

export const getImage = (
    photo: PhotoType, order: OrderType,
    userPhoto?: string | null
): any => {
    let canvas = new fabric.Canvas('c', {
        hoverCursor: 'pointer',
        selection: false,
        backgroundColor: 'white'
    });

    if (order.size == OrderSize.SIZE_10X10) {
        globalHeight = globalWidth;
    }

    canvas.setWidth(globalWidth);
    canvas.setHeight(globalHeight);

    return new Promise(async resolve => {
        const imgs: PhotoSetting[] = [
            {
                url: 'file:///Users/admin/PycharmProjects/cloudrint-new/generator/src/images/insta_send.png',
                settings: {
                    left: percentsToPixels(globalWidth, 89.58),
                    top:  percentsToPixels(globalHeight, 78.25),
                    zIndex: 6,
                    scaleX: 0.15 * scaleC,
                    scaleY: 0.15 * scaleC,
                }
            }, {
                url: 'file:///Users/admin/PycharmProjects/cloudrint-new/generator/src/images/insta_dots.png',
                settings: {
                    left: percentsToPixels(globalWidth, 87.5),
                    top:  percentsToPixels(globalHeight, 5.29),
                    zIndex: 5,
                    scaleX: 0.23 * scaleC,
                    scaleY: 0.23 * scaleC,
                }
            }, {
                url : 'file:///Users/admin/PycharmProjects/cloudrint-new/generator/src/images/insta_icons.png',
                settings: {
                    left: percentsToPixels(globalWidth, 6.25),
                    top:  percentsToPixels(globalHeight, 78.25),
                    zIndex: 6,
                    scaleX: 0.15 * scaleC,
                    scaleY: 0.15 * scaleC,
                }
            },
            {
                url : userPhoto ? userPhoto : 'file:///Users/admin/PycharmProjects/cloudrint-new/generator/src/images/user.png',
                settings: {
                    left: percentsToPixels(globalWidth, 5.41),
                    top:  percentsToPixels(globalHeight, 2.18),
                    zIndex: 5,
                    scaleX: 0.17 * scaleC,
                    scaleY: 0.17 * scaleC,
                }
            }
        ];

        const endFunct = () => {
            const rect = new fabric.Rect({
                left: -1,
                top: -1,
                width: globalWidth + 1,
                height: percentsToPixels(globalHeight, 12.5),
                fill: 'white',
            });
            canvas.add(rect);
            canvas.moveTo(rect, 6);

            const line = new fabric.Line([
                -1,
                percentsToPixels(globalHeight, 12.5),
                globalWidth + 1,
                percentsToPixels(globalHeight, 12.5)
            ], {
                stroke: '#999',
                strokeWidth: 0.5,
                selectable: false,
                evented: false,
            });
            canvas.add(line);
            canvas.moveTo(line, 6);

            const rect1 = new fabric.Rect({
                left: -1,
                top: percentsToPixels(globalHeight, 73.52),
                width: globalWidth + 1,
                height: percentsToPixels(globalHeight, 25),
                fill: 'white',
            });
            canvas.add(rect1);
            canvas.moveTo(rect1, 6);

             let likesText = new fabric.Text(photo.likes + ' отметок "Нравится"', {
                 fontWeight: 'bold',
                 fontSize: 9 * scaleC,
                 left: percentsToPixels(globalWidth, 6.25),
                 top:  percentsToPixels(globalHeight, 86.12),
                 fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif',
                 fill: '#262626'
             });
             canvas.add(likesText);

             let date = new Date(parseInt(photo.date)*1000);
             let year = date.getFullYear();
             let month = getMonthName(date.getMonth());
             let day = date.getDate();

             let dateText = new fabric.Text(day + ' ' + month + ' ' + year + ' г.', {
                 fontWeight: 'bold',
                 fontSize: 9 * scaleC,
                 top: percentsToPixels(globalHeight, 93.12),
                 left: percentsToPixels(globalWidth, 6.25),
                 fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif',
                 fill: '#8e8e8e'
             });
             canvas.add(dateText);
        };

        const setImage = (
            photoSettings: PhotoSetting, urlArray: PhotoSetting[],
            imgArray: any[], endFunct: () => void, stopRec?: boolean
        ) => {
            fabric.Image.fromURL(photoSettings.url, (img) => {
                let img1 = img.set(photoSettings.settings);
                let imgArray1 = [...imgArray, img1];

                if (urlArray.length > 0) {
                    setImage(urlArray[0], urlArray, imgArray1, endFunct);
                    urlArray.shift();
                } else {
                    if (!stopRec) {
                        setImage(photoSettings, [], imgArray1, endFunct, true);
                    }
                    if (stopRec) {
                        for (let photow of imgArray) {
                            canvas.add(photow);
                            if (photow.onCenter) {
                                canvas.centerObject(photow);

                                let checkH = photow.height * photow.scaleX;

                                if (photow.rotate) {
                                     checkH = photow.width * photow.scaleX;
                                }

                                let offsetImage = 0;

                                if (checkH < percentsToPixels(globalHeight, 65.62)) {
                                    offsetImage = (percentsToPixels(globalHeight, 65.62) - checkH) / 2;
                                }

                                photow.top = percentsToPixels(globalHeight, 12.5) + offsetImage;
                            }
                            if (photow.zIndex) {
                                canvas.moveTo(photow, photow.zIndex);
                            }
                        }
                        endFunct();
                        for (let photow of imgArray) {
                            if (photow.zIndex) {
                                canvas.moveTo(photow, photow.zIndex);
                            }
                        }

                        canvas.renderAll();
                        photo.preview = canvas.toDataURL();
                        resolve(photo);
                    }
                }
            });
        };

        if (order.template == OrderTemplate.INSTAGRAM) {
            let instagramHeight = percentsToPixels(globalHeight, 62.5);

            if (photo.rotated) {
                instagramHeight = globalWidth;
            }

            photo.content = await resizeImage(photo.content, globalWidth, instagramHeight);

            setImage({
                url: photo.content,
                settings: {
                    scaleX: photo.scale,
                    scaleY: photo.scale,
                    onCenter: true,
                    zIndex: 4,
                    angle: photo.rotated ? 90 : 0,
                    rotate: photo.rotated
                }
            }, imgs, [], endFunct);
        } else {
            if (photo.image ===OrderImage.BY_WIDTH) {
                photo.content = await resizeImage(photo.content, globalWidth, globalHeight);
            }

            fabric.Image.fromURL(photo.content, (img: any) => {
                if (order.size == OrderSize.SIZE_10X10 && photo.rotated) {
                    img.angle = 90
                }

                img.scaleX = photo.scale;
                img.scaleY = photo.scale;
                img.hasControls= false;
                img.name = "resized";
                canvas.add(img);
                canvas.centerObject(img);
                canvas.renderAll();
                photo["preview"] = canvas.toDataURL();

                resolve(photo);
            });
        }
    });
};

export const getContentImageByUrl = (
    url: string,
    width: number,
    height: number
): any => {
    let canvas = new fabric.Canvas("c_p", {
        hoverCursor: 'pointer',
        selection: false,
        backgroundColor: 'white'
    });

    canvas.setWidth(width);
    canvas.setHeight(height);

    return new Promise(resolve => {
        fabric.Image.fromURL(url, (image: any) => {
            canvas.add(image);
            canvas.centerObject(image);
            canvas.renderAll();
            const base64 = canvas.toDataURL().split(',')[1];
            resolve(base64);
        }, { crossOrigin: 'Anonymous' });
    });
};

export const resizeImage = (
    content: any,
    maxWidth : number,
    maxHeight: number
): any => {
    const Canvas = require('canvas');
    const Image = Canvas.Image;

    return new Promise(async function(resolve){
        let width = 0;
        let height = 0;
        let i = new Image();
        let img = new Image();

        i.onload = function(){
            width = i.width;
            height = i.height;
        };
        i.src = content;

        img.onload = function() {
            const canvas = new Canvas.createCanvas();
            const ctx: any = canvas.getContext('2d');
            let ratio = Math.min(maxWidth / width, maxHeight / height);

            canvas.width = width * ratio;
            canvas.height = height * ratio;
            ctx.drawImage(img, 0, 0, width * ratio, height * ratio);

            resolve(canvas.toDataURL());
        };

        img.src = content;
    })

};

export const percentsToPixels = (
    width: number,
    percent: number
): number => {
    return (width / 100) * percent;
};

export default getImage;