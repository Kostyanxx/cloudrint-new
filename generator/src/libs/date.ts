export const getMonthName = (month: number): string => {
    let months = [
       'января',
       'февраля',
       'марта',
       'апреля',
       'мая',
       'июня',
       'июля',
       'августа',
       'сентября',
       'ноября',
       'декабря',
    ];

    return months[month-1];
};