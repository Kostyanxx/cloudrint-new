import amqp from 'amqplib/callback_api';
import {getImage} from "./libs/image";
import {PhotoType} from "./types/PhotoType";
import {OrderTemplate} from "./enums/OrderEnum";
import fs from 'fs';

amqp.connect('amqp://guest:guest@localhost:5672//', (error0: any, connection: any) => {
    if (error0) {
        console.log('err0000');
        throw error0;
    }
    connection.createChannel(function(error1: any, channel: any) {
        if (error1) {
            console.log('err111');
            throw error1;
        }

        let queue = 'photo';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(" [*] Waiting for messages", queue);

        channel.consume(queue, async (msg: any) => {
            try {
                let photo: any = JSON.parse(msg.content.toString());
                const file = '../../server/' + photo.path + '/original/' + photo.filename;
                const content = fs.readFileSync(file, 'utf8');
                let userPhoto = null;
                let order = photo['order'];

                if (order.template == OrderTemplate.INSTAGRAM) {
                    //userPhoto = fs.readFileSync('../../server/' + photo.path + '/user.png', 'utf8');
                }

                photo = {...photo, content : 'data:image/jpeg;base64,' + content};

                const image: PhotoType = await getImage(photo, order, userPhoto);
                const base64Image = image['preview'].split(';base64,').pop();
                const dirName = '../archives/' + order.id + '/';

                fs.access(dirName, function(err) {
                  if (err && err.code === 'ENOENT') {
                    fs.mkdir(dirName, function () {
                        console.log('can not create dir', dirName);
                    });
                  }
                  fs.writeFile(dirName +photo.filename + '.jpg', base64Image, {encoding: 'base64'}, function() {
                    //console.log('File created', err);
                  });
                });

            }  catch(e) {
                console.log('errorrrr', e)
            }
        }, {
            noAck: true
        });
    });
});