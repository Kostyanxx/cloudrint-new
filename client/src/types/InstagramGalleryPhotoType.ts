export interface InstagramGalleryPhotoType {
    id?: number,
    src: string,
    thumbnail: string,
    thumbnailWidth: number,
    thumbnailHeight: number,
    isSelected: boolean,
}