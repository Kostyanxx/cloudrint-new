export declare type Photos = PhotoType[]

export interface PhotoType {
    id: number,
    content: any,
    preview: any,
    image: number,
    scale: any,
    date: string,
    likes: number,
    rotated: boolean
}