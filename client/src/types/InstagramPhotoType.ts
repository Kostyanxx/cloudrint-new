export interface InstagramPhotoType {
    id?: number,
    location : string,
    username: string,
    user_picture: string,
    likes: number,
    url: string,
    width: number,
    height: number,
    date: string,
    content?: string
}