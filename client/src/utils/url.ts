const objectToUrl = (object: any): string => {
    return Object.keys(object).map((key) => {
        return key + '=' + object[key];
    }).join('&');
};

export default objectToUrl