import BaseService from './BaseService'

class OrderService extends BaseService {
    getOrder(id: number) {
        return this.sendRequest('/clients/orders/' + id, 'GET', true);
    }

    createOrder(data: any) {
        return this.sendRequest(
            '/clients/orders', 'POST',
            true, false, data
        );
    }

    saveOrder(id: number, data: any) {
        return this.sendRequest(
            '/clients/orders/' + id, 'PATCH',
            true, false, data
        );
    }
}

export const orderService = new OrderService();