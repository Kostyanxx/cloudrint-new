import BaseService from './BaseService'
import { PhotoType } from '../types/PhotoType'
import { InstagramPhotoType } from '../types/InstagramPhotoType'

class PhotoService extends BaseService {
    getPhotos(orderId: number, page: number) {
        return this.sendRequest(
            '/clients/orders/' + orderId + '/photos' ,
            'GET', true, {
                page
            });
    }

    savePhoto(photo: PhotoType) {
        return this.sendRequest(
            '/clients/orders/photo/' + photo["id"], 'PATCH', true, false, photo);
    }

    deletePhoto(photoId: number) {
        return this.sendRequest(
            '/clients/orders/photo/' + photoId, 'DELETE', true);
    }

    getInstagramPhotos(token: string) {
        return this.sendRequest(
            'https://api.instagram.com/v1/users/self/media/recent',
            'GET',
            false, {
                access_token: token,
                count: 12
            });
    }

    saveInstagramPhoto(orderId: number, photo: InstagramPhotoType) {
        return this.sendRequest(
            '/clients/orders/' + orderId + '/photo', 'POST', true, false, photo);
    }

    saveInstagramUserPhoto(orderId: number, content: any) {
        return this.sendRequest(
            '/clients/orders/' + orderId + '/photo/user', 'POST', true, false, {content});
    }

    getInstagramUserPhoto(orderId: number) {
        return this.sendRequest(
            '/clients/orders/' + orderId + '/photo/user', 'GET', true, false, false);
    }
}

export const photoService = new PhotoService();