import BaseService from './BaseService'

class AuthService extends BaseService {
    createToken() {
        return this.sendRequest(
            'http://localhost:8000/api/clients/auth',
            'POST',
            false
        );
    }

    checkToken() {
        return this.sendRequest('/clients/auth', 'GET', true);
    }
}

export const authService = new AuthService();