import objectToUrl from '../utils/url';
import history from "../utils/history";

export default class BaseService {
    apiUrl: string;
    headers: any;

    constructor() {
        const token = localStorage.getItem('token');

        this.apiUrl = 'http://localhost:8000/api';
        this.headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        };
    }

    getRequestSettings(
        url: string, method: string,
        isApi?: boolean, params?: any,
        headers?: any
    ) {
        let requestObject: any = {
            method
        };

        if (isApi) {
            requestObject['headers'] = this.headers;
        }
        if (headers) {
            requestObject['headers'] = {
                ...requestObject['headers'],
                ...headers
            };
        }

        return requestObject;
    }

    async sendRequest(
        url: string, method: string,
        isApi?: boolean, params?: any,
        body?: any, headers?: any
    ) {
        const requestObject = this.getRequestSettings(
            url, method, isApi, params, headers
        );

        if (isApi) {
            url = this.apiUrl + url;
        }
        if (params) {
            url = `${url}?${objectToUrl(params)}`;
        }
        if (body) {
            requestObject['body'] = JSON.stringify(body);
        }

        let response = await fetch(url, requestObject);

        if (response.status === 401) {
            history.push('/login');
        }

        console.log(response.status)

        return response.json();
    }
}