export enum OrderImage {
    BY_WIDTH = 1,
    FULL = 2
}

export enum OrderSize {
    SIZE_10X15 = 1,
    SIZE_10X10 = 2
}

export enum OrderTemplate {
    MEDIA = 1,
    INSTAGRAM = 2
}

export enum OrderEnumType {
    DEFAULT = 1,
    INSTAGRAM = 2
}