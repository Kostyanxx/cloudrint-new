import CircularProgress from "@material-ui/core/CircularProgress";
import React from "react";

interface Props {
    mobile?: boolean
}

const Loader = (props: Props) => (
    <div>
        <div className="loader">
            <div className="loader-box"
                style={{marginTop: props.mobile ? '60%' : '20%'}}>
                <CircularProgress size={70}/>
            </div>
        </div>
    </div>
);

export default Loader;