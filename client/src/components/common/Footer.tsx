import React from "react";
import {observer} from "mobx-react";
import {AppContext} from "../../stores/Stores";

const Footer = observer(() => {
    const {pageStore} = React.useContext(AppContext);

    return (
        <div>
            {pageStore.deviceVersion == "desktop" && (
                <div className="row">
                    <div className="col mt-2">
                        <div className="d-block" style={{height: 70}}>
                            <div className="row">
                                <div className="col-2">
                                    <b style={{
                                        color: '#b9bbbd',
                                        cursor: 'pointer',
                                        fontSize: 16
                                    }}>ОФЕРТА</b>
                                </div>
                                <div className="col">
                                    <b style={{
                                        color: '#b9bbbd',
                                        cursor: 'pointer',
                                        fontSize: 16
                                    }}>ОПЛАТА И ДОСТАВКА</b>
                                </div>
                                <div className="col">
                                    <b style={{
                                        color: '#b9bbbd',
                                        cursor: 'pointer',
                                        fontSize: 16,
                                        float: 'right'
                                    }}>&copy; 2019 CLOUDRINT</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            {pageStore.deviceVersion == "mobile" && (
                <div className="row">
                    <div className="col mt-5">
                        <div className="d-block"
                             style={{height: 70, textAlign: 'center', marginBottom: 40, fontSize: 14}}>
                            <div className="row">
                                <div className="col-3">
                                    <b style={{
                                        color: '#b9bbbd',
                                        cursor: 'pointer',
                                    }}>ОФЕРТА</b>
                                </div>
                                <div className="col text-right">
                                    <b style={{
                                        color: '#b9bbbd',
                                        cursor: 'pointer',
                                    }}>ОПЛАТА И ДОСТАВКА</b>
                                </div>
                            </div>
                            <div className="row mt-4">
                                <div className="col text-center">
                                    <b style={{
                                        color: '#b9bbbd',
                                        cursor: 'pointer',
                                    }}>&copy; 2019 CLOUDRINT</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
});

export default Footer;