import React, {useEffect, useState} from "react";
import {observer} from "mobx-react";
import {AppContext} from "../../stores/Stores";
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from "@material-ui/core/CircularProgress";

interface Props {
    url?: string,
}

export const InstagramUploader = observer((props: Props) => {
    const {pageStore, photoStore} = React.useContext(AppContext);

    useEffect(() => {}, []);

    return (
        <div>
            <Dialog
                open={true}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <div>
                    <DialogContent>
                        <div className="pt-4 pb-2" style={{width: 70, margin: '0 auto'}}>
                            <CircularProgress size={70}/>
                        </div>
                        <div style={{textAlign: 'center', marginTop: 20}}>
                            <label>
                                загружено {photoStore.instagramUploadedPhotos} из {photoStore.instagramLoadingPhotos} фото
                            </label>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={() => photoStore.instagramCloseUploadModal()}
                            color="primary"
                            className="modal-button-cancel">
                                Отмена
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </div>
    );
});

export default InstagramUploader;