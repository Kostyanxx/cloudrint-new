import React, { useState } from "react";
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {observer} from "mobx-react";
import {AppContext} from "../../stores/Stores";
import Loader from "./Loader";
import Bag from '@material-ui/icons/ShoppingCart';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';

interface Props {
    children: React.ReactNode,
    width?: number
}

export const Page = observer(function (props: Props) {
    const { pageStore } = React.useContext(AppContext);
    const [menu, openMenu] = useState(false);

    if (pageStore.isLoading)
        return <Loader mobile={pageStore.deviceVersion == "mobile"}/>;

    return (
        <div>
            <div className={menu
                ? 'left-menu open-menu' : 'left-menu close-menu'}>
                <List component="nav">
                    <ListItem button>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Заказы" />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Оферта" />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Доставка" />
                    </ListItem>
                </List>
                <Divider/>
                <div style={{marginTop: 20, marginLeft: 20, fontWeight: 'bold', color: 'black'}}>
                    <div>
                        &copy; 2019 CloudRint
                    </div>
                </div>
            </div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col header" style={{position: 'fixed',zIndex: 100, background: 'white'}}>
                        <div>
                            <IconButton
                                style={{marginTop: 7}}
                                className="float-left"
                                onClick={() => {openMenu(!menu)}}>
                                <MenuIcon style={{fontSize: 32}}/>
                            </IconButton>
                            <div className="d-block float-left header-logo">
                                <b style={{color: '#efb476'}}>Cloud</b>
                                Rint
                            </div>
                        </div>
                        <IconButton className="icon-account float-right" color="inherit">
                            <Bag/>
                        </IconButton>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className="d-block page-box" style={{width: props.width, marginTop: 80}}>
                            {props.children}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
});