import React, {useEffect, useState} from "react";
import {observer} from "mobx-react";
import {AppContext} from "../../stores/Stores";
import LinearProgress from '@material-ui/core/LinearProgress';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import axios from "axios/index";

interface Props {
    sizes: any,
    url: string,
    redirect?: string,
    fields: any,
    beforeSelect: () => void
}

let files: any = [];
let CancelToken = axios.CancelToken;
let source = CancelToken.source();
let config = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    cancelToken: source.token
};

export const Uploader = observer(function (props: Props) {
    const {pageStore, orderStore} = React.useContext(AppContext);
    const [modal, setModal] = useState(false);
    const [filesCount, setFilesCount] = useState(0);
    const [currentLoadFile, setCurrentLoadFile] = useState(1);
    const [currentLoadFilePer, setCurrentLoadFilePer] = useState(1);
    const [loadUserImageModal, setLoadUserImageModal] = useState(false);
    const [loading, setLoading] = useState(false);
    const {sizes} = props;

    useEffect(() => {
        if (currentLoadFile < filesCount && !loading) {
            loadImage(currentLoadFile);
            setCurrentLoadFile(currentLoadFile + 1);
            setCurrentLoadFilePer(0);
        }
        if (currentLoadFile === filesCount && !loading) {
            props.beforeSelect();
            reset();
        }
    }, [currentLoadFile, filesCount, loading]);

    const reset = () => {
        setModal(false);
        setFilesCount(0);
        setCurrentLoadFile(1);
        setCurrentLoadFilePer(1);
        setLoadUserImageModal(false);
        setLoading(false);
        files = [];
        CancelToken = axios.CancelToken;
        source = CancelToken.source();
        config = {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            cancelToken: source.token
        };
    };

    const loadImage = (file: any) => {
        let axiosInstance = axios.create(config);
        let fileUpl = files[file];
        let formData = new FormData();
        for (let i in props.fields) {
            let field = props.fields[i];
            formData.append(field["name"], field["value"]);
        }

        formData.append('file', fileUpl, fileUpl.name);
        setLoading(true);
        axiosInstance.put(props.url, formData, {
            onUploadProgress: progressEvent => {
                let per = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                setCurrentLoadFilePer(per);
            }
        }).then(result => {
            if (result.data && result.data["error"]) {
                //this.props.addNote("Ошибка загрузки файла " + fileUpl.name);
                setModal(false);
                setLoadUserImageModal(false);
            }
            if (result.data["photo_id"]) {
                console.log('loaded')
                setLoading(false);
            } else {
                //this.props.addNote("Ошибка загрузки файла" + fileUpl.name);
            }
        }).catch(function (thrown) {
             if (axios.isCancel(thrown)) {
                CancelToken = axios.CancelToken;
                source = CancelToken.source();
                reset();
             }
        });
    };

    const addUserImages = (e: any) => {
        let data = e.target.files;
        for (let i = 0; i <= data.length - 1; i++) {
            let size = data[i].size / 1000000;
            if (size > 3) {
                /*this.props.addNote("файл "
                    + data[i].name + " имеет размер более 10Mb");*/
            } else {
                files.push(data[i]);
            }
        }
        if (files.length > 0) {
            setFilesCount(files.length);
            loadImage(0);
            setModal(true);
            setLoadUserImageModal(true);
        }
    };

    return (
        <div>
            <input type="file"
                   accept="image/*"
                   name="upload"
                   multiple
                   onChange={(e) => addUserImages(e)}
                   style={{
                       ...sizes,
                       opacity: 0,
                       zIndex: 2,
                       position: 'absolute',
                       cursor: 'pointer',
                       fontSize: 0
                   }}/>
                {modal ? (<Dialog
                    open={true}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    {loadUserImageModal ?
                        <div>
                            <DialogContent>
                                <div style={{textAlign: 'center', marginTop: 20}}>
                                    <label>загружаю {currentLoadFile} из {filesCount} файлов</label>
                                </div>
                                {currentLoadFilePer > 0 ?
                                    <LinearProgress
                                        variant="determinate"
                                        value={currentLoadFilePer}
                                        style={{marginTop: 20}}/> : null
                                }
                                <div style={{textAlign: 'center', marginTop: 20}}>
                                    {currentLoadFilePer} %
                                </div>
                            </DialogContent>
                            <DialogActions>
                                <Button color="primary"
                                        onClick={() => source.cancel()}
                                        className="modal-button-cancel">
                                    Отмена
                                </Button>
                            </DialogActions>
                        </div> : null}
                </Dialog>) : null}
        </div>
    );
});

export default Uploader;