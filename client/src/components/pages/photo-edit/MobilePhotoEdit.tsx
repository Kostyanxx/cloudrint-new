import {observer} from "mobx-react";
import React, {useState} from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import ArrowBack from '@material-ui/icons/ArrowBack';
import ZoomIn from '@material-ui/icons/ZoomIn';
import ZoomOut from '@material-ui/icons/ZoomOut';
import Delete from '@material-ui/icons/Delete';
import getImage from "../../../utils/image";
import Button from "@material-ui/core/Button";

export const MobilePhotoEdit = observer(() => {
    const { pageStore, photoStore, orderStore } = React.useContext(AppContext);
    const [img, setImg] = useState(photoStore.editedPhoto["preview"]);

    const changeZoom = async (value: number) => {
        let editedPhoto = photoStore.editedPhoto;
        editedPhoto.scale = editedPhoto.scale + value;
        photoStore.setEditedPhoto(editedPhoto);
        const photo = await getImage(
            editedPhoto,
            orderStore.order,
            photoStore.instagramUserPhoto
        );
        setImg(photo["preview"]);
    };

    return (
        <div>
            <Page width={290}>
                <div className="container mt-4">
                    <div className="row">
                        <div className="col">
                            <ArrowBack
                                className="float-left"
                                onClick={() => pageStore.goBack()}/>
                            <div className="float-left ml-3">
                                НАСТРОЙКА <b style={{color: '#efb476'}}>ПЕЧАТИ </b>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <ZoomIn className="arrow-icon" onClick={() => changeZoom(0.1)}/>
                            <ZoomOut className="arrow-icon ml-2" onClick={() => changeZoom(-0.1)}/>
                            <Delete className="arrow-icon ml-2" onClick={() => photoStore.deletePhoto()}/>
                        </div>
                    </div>
                    <div className="row mt-4 mb-4">
                        <div className="col-11">
                            <img src={img} className="canvas-photo-edit" style={{height: 350}}/>
                        </div>
                    </div>
                    <div className="row mt-5 mb-5">
                        <div className="col">
                            <Button className="p-3 float-right"
                                    onClick={() => {
                                        photoStore.savePhoto()
                                    }}
                                    style={{border: 'solid 2px #f2c492', width: '100%'}}>
                                СОХРАНИТЬ
                            </Button>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});