import {observer} from "mobx-react";
import React, {useEffect, useState} from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import ZoomIn from '@material-ui/icons/ZoomIn';
import ZoomOut from '@material-ui/icons/ZoomOut';
import Delete from '@material-ui/icons/Delete';
import getImage from "../../../utils/image";
import {OrderTemplate} from '../../../enums/OrderEnum';

export const DesktopPhotoEdit = observer(() => {
    const { pageStore, photoStore, orderStore } = React.useContext(AppContext);
    const [img, setImg] = useState(photoStore.editedPhoto["preview"]);

    useEffect(() => {
        init();
    }, []);

    const init = async () => {
        let editedPhoto = photoStore.editedPhoto;
        await getImage(
            editedPhoto,
            orderStore.order,
            photoStore.instagramUserPhoto
        );
    };

    const changeZoom = async (value: number) => {
        let editedPhoto = photoStore.editedPhoto;
        editedPhoto.scale = editedPhoto.scale + value;
        photoStore.setEditedPhoto(editedPhoto);
        const photo = await getImage(
            editedPhoto,
            orderStore.order,
            photoStore.instagramUserPhoto
        );
        setImg(photo["preview"]);
    };

    return (
        <div>
            <Page width={590}>
                <div className="container pl-4 mt-4">
                    <div className="row mt-4">
                        <div className="col">
                            НАСТРОЙКА <b style={{color: '#efb476'}}>ПЕЧАТИ </b>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <ArrowBack className="float-left d-inline-block"/>
                            <div className="left d-inline-block"
                                 style={{paddingLeft: 4, cursor: 'pointer'}}
                                 onClick={() => pageStore.goBack()}>
                                НАЗАД
                            </div>
                        </div>
                        <div className="col pr-0">
                            <ArrowForward className="float-right d-inline-block"/>
                            <div className="float-right d-inline-block"
                                 onClick={() => photoStore.savePhoto()}
                                 style={{paddingRight: 4, cursor: 'pointer'}}>
                                СОХРАНИТЬ
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5 mb-4">
                        <div className="col-1">
                            <div>
                                <ZoomIn className="arrow-icon" onClick={() => changeZoom(0.1)}/>
                            </div>
                            <div>
                                <ZoomOut className="arrow-icon" onClick={() => changeZoom(-0.1)}/>
                            </div>
                            <div>
                                <Delete className="arrow-icon" onClick={() => photoStore.deletePhoto()}/>
                            </div>
                        </div>
                        <div className="col-11">
                            <div style={{margin: '0 auto', width: 236}} className="canvas-photo-edit">
                                {/*{orderStore.order.template==OrderTemplate.MEDIA && (
                                    <canvas id="c"/>
                                )}*/}
                                {(
                                    <img src={img} className="canvas-photo-edit"/>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});