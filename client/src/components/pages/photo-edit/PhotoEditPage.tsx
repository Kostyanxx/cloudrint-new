import {observer} from "mobx-react";
import React, {useEffect} from "react";
import {AppContext} from "../../../stores/Stores";
import {DesktopPhotoEdit} from "./DesktopPhotoEdit";
import {MobilePhotoEdit} from "./MobilePhotoEdit";

export const PhotoEditPage = observer(function () {
    const { pageStore, photoStore } = React.useContext(AppContext);

    useEffect(() => {
        if (!photoStore.order) {
            pageStore.goBack();
        }
    });

    if (pageStore.deviceVersion == "mobile") {
        return <MobilePhotoEdit/>;
    }

    return <DesktopPhotoEdit/>;
});