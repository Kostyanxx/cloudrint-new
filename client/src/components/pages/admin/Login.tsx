import {observer} from "mobx-react";
import React, {useState} from "react";
import {Page} from "../../common/Page";
import Button from '@material-ui/core/Button';
import {AppContext} from "../../../stores/Stores";
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';

export const Login = observer(function () {
    const {pageStore, orderStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={800}>
                <div className="auth-form">
                    <label className="auth-header">Войти</label>
                    <div className="auth-form-item">
                        <TextField
                            error={false}
                            label="Логин"
                            value={''}
                            name="login"
                            className={"textField"}
                            margin="normal"/>
                        <TextField
                            error={false}
                            label="Пароль"
                            value={''}
                            name="pass"
                            className="textField"
                            type="password"
                            margin="normal"/>
                        <Button className="auth-button">
                            Войти
                        </Button>
                        <div className="auth-check">
                            <div className="auth-switch">
                                <Switch
                                    checked={false}
                                    color="primary"/>
                            </div>
                            <label className="auth-remember-label">запомнить меня</label>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});
