import {observer} from "mobx-react";
import React from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import ArrowForward from '@material-ui/icons/ArrowForward';
import Delete from '@material-ui/icons/Delete';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

export const Orders = observer(() => {
    const {pageStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={850}>
                <div className="container pl-4 mt-4">
                    <div className="row mt-4">
                        <div className="col">
                            ЗАКАЗЫ ДЛЯ <b style={{color: '#efb476'}}> ПЕЧАТИ </b>(34)
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            <div style={{height: 50, border: '2px solid rgb(242, 196, 146)', fontSize: 14, paddingLeft:20, paddingRight: 20}}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 13}}>
                                        234234234234
                                    </div>
                                    <div className="col-2" style={{paddingTop: 8, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <KeyboardArrowDownIcon style={{fontSize: 32}}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            gfhdfhfgh
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <div style={{height: 50, border: '2px solid rgb(242, 196, 146)', fontSize: 14, paddingLeft:20, paddingRight: 20}}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 13}}>
                                        Фотографии 30Х40 - 30шт. (125 руб.)
                                    </div>
                                    <div className="col-2" style={{paddingTop: 12, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <Delete/>
                                            <ArrowForward className="float-right ml-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <div style={{height: 50, border: '2px solid rgb(242, 196, 146)', fontSize: 14, paddingLeft:20, paddingRight: 20}}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 13}}>
                                        Фотографии 30Х40 - 30шт. (125 руб.)
                                    </div>
                                    <div className="col-2" style={{paddingTop: 12, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <Delete/>
                                            <ArrowForward className="float-right ml-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});