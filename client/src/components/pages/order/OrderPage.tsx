import {observer} from "mobx-react";
import React from "react";
import {AppContext} from "../../../stores/Stores";
import {DesktopOrder} from "./DesktopOrder";
import {MobileOrder} from "./MobileOrder";

export const OrderPage = observer(() => {
    const {pageStore} = React.useContext(AppContext);

    if (pageStore.deviceVersion == "mobile") {
        return <MobileOrder/>;
    }

    return <DesktopOrder/>;
});