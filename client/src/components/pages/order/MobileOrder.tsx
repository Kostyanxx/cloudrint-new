import {observer} from "mobx-react";
import React from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import TextField from "@material-ui/core/TextField";
import Checkbox from '@material-ui/core/Checkbox';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import Delete from '@material-ui/icons/Delete';
import history from "../../../utils/history";
import Button from "@material-ui/core/Button";
import Footer from "../../common/Footer";

export const MobileOrder = observer(() => {
    const {pageStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={290}>
                <div className="container mt-4">
                    <div className="row mt-4">
                        <div className="col">
                            <ArrowBack
                                className="float-left"
                                onClick={() => pageStore.goBack()}/>
                            <div className="float-left ml-3">
                                МОИ <b style={{color: '#efb476'}}> ЗАКАЗЫ </b> (3)
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div className="row mt-3">
                                <div className="col">
                                    <TextField placeholder="Имя / Фамилия Получателя" style={{width: '100%'}}/>
                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col">
                                    <TextField placeholder="Телефон или email" style={{width: '100%'}}/>
                                </div>
                            </div>
                            <div className="row mt-4">
                                <div className="col">
                                    <TextField
                                        placeholder="Адрес почты или индекс"
                                        style={{width: '100%'}}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            <div style={{
                                border: '2px solid rgb(242, 196, 146)',
                                fontSize: 14,
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingBottom: 10
                            }}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 17}}>
                                        Фотографии 30Х40 - 30шт. (125 руб.)
                                    </div>
                                    <div className="col-2" style={{paddingTop: 12, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <Delete/>
                                            <ArrowForward className="float-right mt-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <div style={{
                                border: '2px solid rgb(242, 196, 146)',
                                fontSize: 14,
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingBottom: 10
                            }}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 17}}>
                                        Фотографии 30Х40 - 30шт. (125 руб.)
                                    </div>
                                    <div className="col-2" style={{paddingTop: 12, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <Delete/>
                                            <ArrowForward className="float-right mt-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col text-center">
                            ИТОГО : 357 РУБЛЕЙ
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col text-center">
                            ДОСТАВКА : 200 РУБЛЕЙ
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col text-center">
                            <b>К ОПЛАТЕ : 357 РУБЛЕЙ</b>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-2">
                            <Checkbox
                                style={{padding: 0, marginTop: 10}}
                                checked={true}
                                value="checkedA"
                                color="default"
                                inputProps={{
                                    'aria-label': 'secondary checkbox',
                                }}
                            />
                        </div>
                        <div className="col-10">
                            c <a href="#"
                                 style={{marginLeft: 5, marginRight: 5, color: 'rgb(239, 180, 118)'}}>
                            договором-офертой
                        </a> ознакомлен
                        </div>
                    </div>
                    <Button className="p-3 float-right mt-4"
                            onClick={() => {

                            }}
                            style={{border: 'solid 2px #f2c492', width: '100%'}}>
                        ОПЛАТИТЬ
                    </Button>
                    <Footer/>
                </div>
            </Page>
        </div>
    );
});