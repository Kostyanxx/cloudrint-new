import {observer} from "mobx-react";
import React from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import TextField from "@material-ui/core/TextField";
import Checkbox from '@material-ui/core/Checkbox';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import Delete from '@material-ui/icons/Delete';

export const DesktopOrder = observer(() => {
    const {pageStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={750}>
                <div className="container pl-4 mt-4">
                    <div className="row mt-4">
                        <div className="col">
                            ЗАКАЗ ДЛЯ <b style={{color: '#efb476'}}> ПЕЧАТИ </b>
                        </div>
                    </div>
                    <div className="row" style={{marginTop: 40}}>
                        <div className="col">
                            <ArrowBack className="float-left d-inline-block"/>
                            <div className="left d-inline-block"
                                 style={{paddingLeft: 4, cursor: 'pointer'}}
                                 onClick={() => pageStore.goBack()}>
                                НАЗАД
                            </div>
                        </div>
                        <div className="col">
                            <ArrowForward className="float-right d-inline-block"/>
                            <div className="float-right d-inline-block">ОПЛАТИТЬ</div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div className="row mt-3">
                                <div className="col-6">
                                    <TextField placeholder="Имя / Фамилия Получателя" style={{width: '100%'}}/>
                                </div>
                                <div className="col-6">
                                    <TextField placeholder="Телефон или email" style={{width: '100%'}}/>
                                </div>
                            </div>
                            <div className="row mt-4">
                                <div className="col-12">
                                    <TextField
                                        placeholder="Адрес почты или индекс / Почта России"
                                        style={{width: '100%'}}/>
                                </div>
                            </div>
                            <div className="row pl-2 mt-3">
                                <div className="d-block">
                                    <Checkbox
                                        checked={true}
                                        value="checkedA"
                                        color="default"
                                        inputProps={{
                                            'aria-label': 'secondary checkbox',
                                        }}
                                    />
                                </div>
                                <div className="d-block" style={{paddingTop: 10}}>
                                    c <a href="#"
                                         style={{marginLeft: 5, marginRight: 5, color: 'rgb(239, 180, 118)'}}>
                                    договором-офертой
                                </a> ознакомлен
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <div style={{height: 50, border: '2px solid rgb(242, 196, 146)', fontSize: 14, paddingLeft:20, paddingRight: 20}}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 13}}>
                                        Фотографии 30Х40 - 30шт. (125 руб.)
                                    </div>
                                    <div className="col-2" style={{paddingTop: 12, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <Delete/>
                                            <ArrowForward className="float-right ml-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <div style={{height: 50, border: '2px solid rgb(242, 196, 146)', fontSize: 14, paddingLeft:20, paddingRight: 20}}>
                                <div className="row">
                                    <div className="col-10" style={{paddingTop: 13}}>
                                        Фотографии 30Х40 - 30шт. (125 руб.)
                                    </div>
                                    <div className="col-2" style={{paddingTop: 12, cursor: 'pointer'}}>
                                        <div className="d-block float-right" style={{color: '#808080'}}>
                                            <Delete/>
                                            <ArrowForward className="float-right ml-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col text-right">
                            ИТОГО : 357 РУБЛЕЙ
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col text-right">
                            ДОСТАВКА : 200 РУБЛЕЙ
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col text-right">
                            <b>К ОПЛАТЕ : 357 РУБЛЕЙ</b>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});