import {observer} from "mobx-react";
import React, {useEffect, useState} from "react";
import {Page} from "../../common/Page";
import Button from '@material-ui/core/Button';
import {AppContext} from "../../../stores/Stores";
import ArrowForward from '@material-ui/icons/ArrowForward';
import SelectAll from '@material-ui/icons/SelectAll';
import Gallery from "../../common/Gallery";
import {InstagramUploader} from "../../common/InstagramUploader";
import ArrowBack from '@material-ui/icons/ArrowBack';

let url = window.location.href;
let accessToken = url.split('=')[1];

export const MobileInstagram = observer(() => {
    const {photoStore, pageStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={290}>
                <div className="container pb-5">
                    <div className="row mt-4">
                        {photoStore.instagramSelectedCount>0 && (
                            <div className="col">
                                <ArrowBack
                                    className="float-left"
                                    onClick={() => pageStore.goBack()}/>
                                <div className="float-left ml-3">
                                    ВЫБРАНО {photoStore.instagramSelectedCount}
                                    <b style={{color: '#efb476'}}> ФОТО ИЗ </b>
                                    {photoStore.instagramGalleryPhotos.length}
                                </div>
                            </div>
                        )}
                        {photoStore.instagramSelectedCount==0 && (
                            <div className="col  pl-0">
                                <ArrowBack
                                    className="float-left"
                                    onClick={() => pageStore.goBack()}/>
                                <div className="float-left ml-3">
                                    ВЫБЕРИТЕ
                                    <b style={{color: '#efb476'}}> ФОТО </b>
                                    ({photoStore.instagramGalleryPhotos.length})
                                </div>
                            </div>
                        )}
                    </div>
                    <div className="row mt-5">
                        <div className="col-10">
                            <div className="d-flex">
                                <div>
                                    <SelectAll className="float-left d-inline-block"/>
                                    <div className="left d-inline-block ml-2"
                                         onClick={() => photoStore.toggleAllInstagramPhoto()}
                                         style={{cursor: 'pointer'}}>
                                        {photoStore.instagramAllSelected ? 'УБРАТЬ' : 'ВЫБРАТЬ'} ВСЕ
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-2">
                            {photoStore.instagramSelectedCount>0 && (
                                <div>
                                    <div className="float-right d-inline-block"
                                         onClick={() => photoStore.instagramLoadPhotos(accessToken)}
                                         style={{paddingRight: 4, cursor: 'pointer'}}>
                                        <ArrowForward className="float-right d-inline-block"/>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="row mt-4 mb-4">
                        <div className="col">
                            <Gallery
                                images={photoStore.instagramGalleryPhotos}
                                onSelectImage={photoStore.selectInstagramPhoto}
                                onClickThumbnail={photoStore.selectInstagramPhoto}
                                enableLightbox={false}/>
                        </div>
                    </div>
                    <div className="row mt-4 mb-4">
                        <div className="col">
                            {photoStore.instagramNextUrl && (
                                <Button
                                    onClick={() => photoStore.getInstagramPhotos(false, true)}
                                    style={{display: 'block', margin: '0 auto', color: '#808080'}}
                                    className="modal-button-cancel"
                                    color="primary">
                                        ЗАГРУЗИТЬ ЕЩЕ ФОТО
                                </Button>
                            )}
                        </div>
                    </div>
                </div>
                {photoStore.instagramLoadModal && (
                    <InstagramUploader/>
                )}
            </Page>
        </div>
    );
});