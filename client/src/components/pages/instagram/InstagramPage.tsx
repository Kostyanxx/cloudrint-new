import {observer} from "mobx-react";
import React, {useEffect} from "react";
import {AppContext} from "../../../stores/Stores";
import {MobileInstagram} from "./MobileInstagram";
import {DesktopInstagram} from "./DesktopInstagram";

let url = window.location.href;
let accessToken = url.split('=')[1];

export const InstagramPage = observer(() => {
    const { pageStore, photoStore} = React.useContext(AppContext);

    useEffect(() => {
        pageStore.startLoading();
        photoStore.getInstagramPhotos(accessToken);
    },[]);

    if (pageStore.deviceVersion == "mobile") {
        return <MobileInstagram/>;
    }

    return <DesktopInstagram/>;
});