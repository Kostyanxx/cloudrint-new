import {observer} from "mobx-react";
import React, {useEffect, useState} from "react";
import {AppContext} from "../../../stores/Stores";
import {DesktopHome} from "./DesktopHome";
import {MobileHome} from "./MobileHome";

export const HomeComponent = observer(() => {
    const {pageStore} = React.useContext(AppContext);

    if (pageStore.deviceVersion == "mobile") {
        return <MobileHome/>;
    }

    return <DesktopHome/>;
});
