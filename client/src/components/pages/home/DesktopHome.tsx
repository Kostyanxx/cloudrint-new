import {observer} from "mobx-react";
import React, {useState} from "react";
import {Page} from "../../common/Page";
import Button from '@material-ui/core/Button';
import {AppContext} from "../../../stores/Stores";
import {OrderEnumType} from '../../../enums/OrderEnum';
import Footer from "../../common/Footer";

export const DesktopHome = observer(function () {
    const {pageStore, orderStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={800}>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div style={{display: 'flex'}}>
                                <div className="d-block" style={{
                                    height: 398,
                                    width: 580,
                                    backgroundImage: "url('https://cdn0.canvaspop.com/XzR0mFUqqO_wQexDdEKG4SAAkpk=/fit-in/1280x745/filters:quality(70)/static.canvaspop.com/v2/images/2016/canvas-prints-hero.jpg')",
                                }}>
                                </div>
                                <div style={{marginLeft: 20}}>
                                    <div style={{marginBottom: 18}}>
                                        МЫ <b style={{color: '#efb476'}}>ПЕЧАТАЕМ</b>
                                    </div>
                                    <div className="bg-dark" style={{
                                        width: 170,
                                        height: 170,
                                        backgroundImage: "url('https://cdn0.canvaspop.com/qixe3mWRwwfvwXHiGlUTwgN_vmw=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canvas.jpg')"
                                    }}/>
                                    <div className="col bg-dark mt-3" style={{
                                        width: 170,
                                        height: 170,
                                        backgroundImage: "url('https://cdn0.canvaspop.com/qixe3mWRwwfvwXHiGlUTwgN_vmw=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canvas.jpg')"
                                    }}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-5 pb-5">
                        <div className="col">
                            <div className="row">
                                <div className="col-7 pr-0">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24}}>
                                            <b style={{color: '#efb476'}}>Картины на</b> холсте
                                        </div>
                                        <div className="d-block mt-4 text-justify pr-3">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                        <div className="d-block">
                                            <Button className="mt-5 p-3"
                                                    onClick={() => {
                                                        orderStore.createOrder(OrderEnumType.INSTAGRAM);
                                                    }}
                                                    style={{border: 'solid 2px #f2c492'}}>
                                                 ВЫБРАТЬ ИЗ ГАЛЕРЕИ
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-5 ">
                                    <div className="d-block bg-dark" style={{height: 270}}>
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={300} height={270}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-5 pb-5">
                        <div className="col">
                            <div className="row">
                                <div className="col-5">
                                    <div className="d-block bg-dark" style={{height: 270}}>
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                                <div className="col-7 pr-0">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24, textAlign: 'right'}}>
                                            <b style={{color: '#efb476'}}>Мои </b> фотографии
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                        <div className="d-block float-right">
                                            <Button
                                                className="mt-5 p-3 "
                                                onClick={() => {
                                                    orderStore.createOrder(OrderEnumType.DEFAULT);
                                                }}
                                                style={{border: 'solid 2px #f2c492'}}>
                                                ДОБАВИТЬ (От 8 руб / фото)
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-5">
                        <div className="col">
                            <div className="row">
                                <div className="col-7 pr-0">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                                <div className="col-5">
                                    <div className="d-block float-right" style={{height: 270}}>
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-3">
                        <div className="col">
                            <div className="row">
                                <div className="col-5">
                                    <div className="d-block" style={{height: 270}}>
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                                <div className="col-7 pr-0">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24, textAlign: 'right'}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-5">
                        <div className="col">
                            <div className="row">
                                <div className="col-7 pr-0">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                                <div className="col-5">
                                    <div className="d-block float-right" style={{height: 270}}>
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>

            </Page>
        </div>
    );
});
