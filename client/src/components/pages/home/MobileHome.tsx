import {observer} from "mobx-react";
import React, {useState} from "react";
import {Page} from "../../common/Page";
import Button from '@material-ui/core/Button';
import {AppContext} from "../../../stores/Stores";
import {OrderEnumType} from '../../../enums/OrderEnum';
import Footer from "../../common/Footer";

export const MobileHome = observer(function () {
    const {pageStore, orderStore} = React.useContext(AppContext);

    return (
        <div>
            <Page width={290}>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div style={{display: 'flex'}}>
                                <div className="d-block" style={{
                                    height: 350,
                                    width: 300,
                                    backgroundImage: "url('https://cdn0.canvaspop.com/XzR0mFUqqO_wQexDdEKG4SAAkpk=/fit-in/1280x745/filters:quality(70)/static.canvaspop.com/v2/images/2016/canvas-prints-hero.jpg')",
                                }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-5">
                        <div className="col">
                            <div style={{fontSize: 18, textAlign: 'center'}}>
                                <b style={{color: '#efb476'}}>Instagram</b> фотографии
                            </div>
                            <img
                                style={{marginTop: 30}}
                                src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                width={270}/>
                            <div className="d-block mt-4 text-justify">
                                Сертифицированное в архивном виде и не содержащее OBA, наше
                                20,5-миллиметровое ярко-белое,
                                устойчивое полихлопковое матовое полотно с бескислотным покрытием
                            </div>
                            <div className="d-block">
                                <Button className="mt-4 p-3"
                                        onClick={() => {
                                            orderStore.createOrder(OrderEnumType.INSTAGRAM);
                                        }}
                                        style={{border: 'solid 2px #f2c492', width: '100%'}}>
                                    ДОБАВИТЬ
                                    (От 8 руб / фото)
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-5">
                        <div className="col">
                            <div style={{fontSize: 18, textAlign: 'center'}}>
                                <b style={{color: '#efb476'}}>Сохраненные</b> фото
                            </div>
                            <img
                                style={{marginTop: 30}}
                                src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                width={270}/>
                            <div className="d-block mt-4 text-justify">
                                Сертифицированное в архивном виде и не содержащее OBA, наше
                                20,5-миллиметровое ярко-белое,
                                устойчивое полихлопковое матовое полотно с бескислотным покрытием
                            </div>
                            <div className="d-block">
                                <Button
                                    className="mt-4 p-3"
                                    onClick={() => {
                                        orderStore.createOrder(OrderEnumType.DEFAULT);
                                    }}
                                    style={{border: 'solid 2px #f2c492', width: '100%'}}>
                                    ДОБАВИТЬ
                                    (От 8 руб / фото)
                                </Button>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            </Page>
        </div>
    );
});
