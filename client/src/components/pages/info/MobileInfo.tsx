import {observer} from "mobx-react";
import React from "react";
import {Page} from "../../common/Page";
import Footer from "../../common/Footer";
import ArrowBack from '@material-ui/icons/ArrowBack';

export const MobileInfo = observer(() => {
    return (
        <div>
            <Page width={290}>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <ArrowBack className="float-left d-inline-block" style={{cursor: 'pointer'}}/>
                            <div className="left d-inline-block ml-2"
                                 style={{paddingLeft: 4}}>
                                ОПЛАТА И <b style={{color: '#efb476'}}> ДОСТАВКА </b>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            ОПЛАТА
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div style={{fontSize: 18}}>
                                <b style={{color: '#efb476'}}>Instagram</b> фотографии
                            </div>
                            <img
                                style={{marginTop: 30}}
                                src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                width={250}/>
                            <div className="d-block mt-4 text-justify">
                                Сертифицированное в архивном виде и не содержащее OBA, наше
                                20,5-миллиметровое ярко-белое,
                                устойчивое полихлопковое матовое полотно с бескислотным покрытием
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div style={{fontSize: 18}}>
                                <b style={{color: '#efb476'}}>Instagram</b> фотографии
                            </div>
                            <img
                                style={{marginTop: 30}}
                                src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                width={250}/>
                            <div className="d-block mt-4 text-justify">
                                Сертифицированное в архивном виде и не содержащее OBA, наше
                                20,5-миллиметровое ярко-белое,
                                устойчивое полихлопковое матовое полотно с бескислотным покрытием
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col text-justify">
                            вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            ДОСТАВКА
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div style={{fontSize: 18}}>
                                <b style={{color: '#efb476'}}>Instagram</b> фотографии
                            </div>
                            <img
                                style={{marginTop: 30}}
                                src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                width={250}/>
                            <div className="d-block mt-4 text-justify">
                                Сертифицированное в архивном виде и не содержащее OBA, наше
                                20,5-миллиметровое ярко-белое,
                                устойчивое полихлопковое матовое полотно с бескислотным покрытием
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div style={{fontSize: 18}}>
                                <b style={{color: '#efb476'}}>Instagram</b> фотографии
                            </div>
                            <img
                                style={{marginTop: 30}}
                                src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                width={250}/>
                            <div className="d-block mt-4 text-justify">
                                Сертифицированное в архивном виде и не содержащее OBA, наше
                                20,5-миллиметровое ярко-белое,
                                устойчивое полихлопковое матовое полотно с бескислотным покрытием
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col text-justify">
                            вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <Footer/>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});