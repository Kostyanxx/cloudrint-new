import {observer} from "mobx-react";
import React, {useEffect} from "react";
import {AppContext} from "../../../stores/Stores";
import {MobileInfo} from "./MobileInfo";
import {DesktopInfo} from "./DesktopInfo";

export const InfoPage = observer(() => {
    const { pageStore, photoStore} = React.useContext(AppContext);

    if (pageStore.deviceVersion == "mobile") {
        return <MobileInfo/>;
    }

    return <DesktopInfo/>;
});