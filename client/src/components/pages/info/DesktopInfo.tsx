import {observer} from "mobx-react";
import React from "react";
import {Page} from "../../common/Page";
import Footer from "../../common/Footer";
import ArrowBack from '@material-ui/icons/ArrowBack';

export const DesktopInfo = observer(() => {
    return (
        <div>
            <Page width={790}>
                <div className="container mt-4 pb-5">
                    <div className="row">
                        <div className="col">
                            <ArrowBack className="float-left d-inline-block" style={{cursor: 'pointer'}}/>
                            <div className="left d-inline-block ml-2"
                                 style={{paddingLeft: 4}}>
                                ОПЛАТА И <b style={{color: '#efb476'}}> ДОСТАВКА </b>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            ОПЛАТА
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div className="row">
                                <div className="col-7">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                                <div className="col-5">
                                    <div className="d-block float-right">
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            <div className="row">
                                <div className="col-5">
                                    <div className="d-block">
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                                <div className="col-7">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24, textAlign: 'right'}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            ДОСТАВКА
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div className="row">
                                <div className="col-7">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                                <div className="col-5">
                                    <div className="d-block float-right">
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div className="row">
                                <div className="col-5">
                                    <div className="d-block">
                                        <img
                                            src="https://cdn0.canvaspop.com/Ntj8t9_MGp52qYi_XOpJTeYTL9M=/fit-in/352x199/filters:quality(70)/static.canvaspop.com/v2/images/why_canon.jpg"
                                            width={236}/>
                                    </div>
                                </div>
                                <div className="col-7 pr-0">
                                    <div className="d-block home-block">
                                        <div style={{fontSize: 24, textAlign: 'right'}}>
                                            <b style={{color: '#efb476'}}>Музейное </b> качество холста.
                                        </div>
                                        <div className="d-block mt-4 text-justify">
                                            Сертифицированное в архивном виде и не содержащее OBA, наше
                                            20,5-миллиметровое ярко-белое,
                                            устойчивое полихлопковое матовое полотно с бескислотным покрытием
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап вапвапвап
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col">
                            <Footer/>
                        </div>
                    </div>
                </div>
            </Page>
        </div>
    );
});