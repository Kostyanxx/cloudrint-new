import {observer} from "mobx-react";
import React, { useEffect } from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import ArrowForward from '@material-ui/icons/ArrowForward';
import Button from '@material-ui/core/Button';
import Add from '@material-ui/icons/Add';
import Settings from '@material-ui/icons/Settings';
import history from "../../../utils/history";
import Uploader from "../../common/Uploader";
import { OrderEnumType, OrderSize } from '../../../enums/OrderEnum';

interface Props {
    order: number
}

export const DesktopPhoto = observer(function (props: Props) {
    const { photoStore, orderStore } = React.useContext(AppContext);
    const orderId: number = props.order;

    return (
        <div>
            <Page width={790}>
                <div className="container pl-4 mt-4">
                    <div className="row mt-4">
                        <div className="col">
                            ФОТОГРАФИИ <b style={{color: '#efb476'}}> 30Х40 </b>({photoStore.count})
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col">
                            <div className="d-block float-left">
                                {orderStore.order.type == OrderEnumType.INSTAGRAM && (
                                    <Add className="arrow-photo"
                                         onClick={() => {
                                             localStorage.setItem('order_id', orderId.toString());
                                             window.location.href = 'https://www.instagram.com/oauth/authorize/?' +
                                                 'client_id=1be32d2bb9124e87a0402693adaf149e' +
                                                 '&redirect_uri=http://localhost:3000/instagram&response_type=token'
                                         }}/>
                                )}
                                {orderStore.order.type == OrderEnumType.DEFAULT && (
                                    <div>
                                        <Uploader
                                            sizes={{width: 30, height: 30}}
                                            url={`http://localhost:8000/api/clients/orders/${orderId}/photo`}
                                            beforeSelect={() => {
                                                photoStore.reset();
                                            }}
                                            fields={[{
                                                name: "order_id", value: orderId
                                            }]}/>
                                        <Add className="arrow-photo"/>
                                    </div>
                                )}
                            </div>
                            <div className="d-block float-left">
                                <Settings
                                    className="arrow-photo"
                                    onClick={() => photoStore.goToSettings()}
                                    style={{marginLeft: 20, cursor: 'pointer'}}/>
                            </div>
                        </div>
                        <div className="col pr-0">
                            <ArrowForward className="float-right d-inline-block"/>
                            <div className="float-right d-inline-block"
                                 onClick={() => history.push('/orders')}
                                 style={{paddingRight: 4, cursor: 'pointer'}}>
                                РАСПЕЧАТАТЬ
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4 mb-4">
                        <div className="col">
                            {photoStore.photos.map((item, i) =>
                                <div className="float-left ml-3" key={i} onClick={() => {
                                    photoStore.setEditedPhoto(item);
                                    history.push('/photo/edit');
                                }}>
                                    <img className="photo-edit-img"
                                         src={item.preview}
                                         height={orderStore.order.size == OrderSize.SIZE_10X10 ? 232 : 310}/>
                                </div>
                            )}
                        </div>
                    </div>
                    {photoStore.hasNext ? (
                        <div className="row mt-4 mb-4">
                            <div className="col">
                                <Button onClick={() => photoStore.nextPage()}
                                        className="load-more">
                                    ПОКАЗАТЬ ЕЩЕ
                                </Button>
                            </div>
                        </div>) : null}
                </div>
            </Page>
        </div>
    );
});