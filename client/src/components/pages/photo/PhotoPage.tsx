import {observer} from "mobx-react";
import React, { useEffect } from "react";
import {AppContext} from "../../../stores/Stores";
import {DesktopPhoto} from "./DesktopPhoto";
import {MobilePhoto} from "./MobilePhoto";

interface Props {
    match: any
}

export const PhotoPage = observer(function (props: Props) {
    const { pageStore, photoStore, orderStore } = React.useContext(AppContext);
    const orderId: number = props.match.params.id;

    useEffect(() => {
        if (orderId !== photoStore.order) {
            orderStore.getOrder(orderId);
            photoStore.getPhotos(orderId);
        }
    },[props.match]);

    if (pageStore.deviceVersion == "mobile") {
        return <MobilePhoto order={orderId}/>;
    }

    return <DesktopPhoto order={orderId}/>;


});