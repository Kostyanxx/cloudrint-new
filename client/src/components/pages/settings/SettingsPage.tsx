import {observer} from "mobx-react";
import React, {useEffect} from "react";
import {AppContext} from "../../../stores/Stores";
import {DesktopSettings} from "./DesktopSettings";
import {MobileSettings} from "./MobileSettings";

interface Props {
    match: any
}

export const SettingsPage = observer((props: Props) => {
    const {pageStore, orderStore} = React.useContext(AppContext);
    const orderId: number = props.match.params.id;

    useEffect(() => {
        orderStore.getOrder(orderId);
    }, [props.match]);

    if (pageStore.deviceVersion == "mobile") {
        return <MobileSettings order={orderId}/>;
    }

    return <DesktopSettings order={orderId}/>;
});