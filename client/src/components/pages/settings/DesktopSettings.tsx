import {observer} from "mobx-react";
import React, {useState, useEffect} from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import history from "../../../utils/history";
import {OrderTemplate, OrderImage, OrderEnumType} from '../../../enums/OrderEnum'
import Uploader from "../../common/Uploader";

interface Props {
    order: number
}

export const DesktopSettings = observer((props: Props) => {
    const {pageStore, orderStore, photoStore} = React.useContext(AppContext);
    const orderId: number = props.order;

    return (
        <div>
            <Page width={700}>
                <div className="container pl-4 mt-4">
                    <div className="row mt-4">
                        <div className="col">
                            ЗАКАЗ ДЛЯ <b style={{color: '#efb476'}}> ПЕЧАТИ </b>
                        </div>
                    </div>
                    <div className="row" style={{marginTop: 40}}>
                        <div className="col">
                            <ArrowBack className="float-left d-inline-block"/>
                            <div className="left d-inline-block"
                                 style={{paddingLeft: 4, cursor: 'pointer'}}
                                 onClick={() => pageStore.goBack()}>
                                НАЗАД
                            </div>
                        </div>
                        <div className="col pr-0">
                            <ArrowForward className="float-right d-inline-block"/>
                            {orderStore.order.type === OrderEnumType.INSTAGRAM && !orderStore.order.hasPhotos && (
                                <div className="float-right d-inline-block"
                                     onClick={() => {
                                         localStorage.setItem('order_id', orderId.toString());
                                         window.location.href = 'https://api.instagram.com/oauth/authorize?client_id=711679642940859&redirect_uri=https://localhost:3000/instagram&scope=user_profile,user_media&response_type=code'
                                     }}
                                     style={{paddingRight: 4, cursor: 'pointer'}}>
                                    ВЫБРАТЬ ФОТО
                                </div>)}
                            {(orderStore.order.type === OrderEnumType.DEFAULT && !orderStore.order.hasPhotos) && (
                                <div>
                                    <div className="float-right d-inline-block"
                                         style={{paddingRight: 4, cursor: 'pointer'}}>
                                        <Uploader
                                            sizes={{width: 136, height: 20}}
                                            url={`http://localhost:8000/api/clients/orders/${orderId}/photo`}
                                            beforeSelect={() => {
                                                orderStore.saveOrder();
                                                history.push('/order/' + orderId + '/photos');
                                            }}
                                            fields={[{
                                                name: "order_id", value: orderId
                                            }]}/>
                                        ВЫБРАТЬ ФОТО
                                    </div>
                                </div>)}
                            {orderStore.order.hasPhotos && (
                                <div>
                                    <div className="float-right d-inline-block"
                                         onClick={() => {
                                             orderStore.saveOrder();
                                             photoStore.reset();
                                             history.push('/order/' + orderId + '/photos');
                                         }}
                                         style={{paddingRight: 4, cursor: 'pointer'}}>
                                        СОХРАНИТЬ
                                    </div>
                                </div>)}
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-4">
                            <div className="row pt-4">
                                <div className="col">
                                    <div className="d-block">
                                        РАЗМЕР
                                    </div>
                                    <Select
                                        style={{padding: 5, width: 200}}
                                        className="pt-4"
                                        onChange={(e) => {
                                            orderStore.changeField("size", e.target.value)
                                        }}
                                        value={orderStore.order.size}>
                                        <MenuItem value={1}>10X15 (8 руб/шт)</MenuItem>
                                        <MenuItem value={2}>10X10 (8 руб/шт)</MenuItem>
                                    </Select>
                                </div>
                            </div>
                            <div className="row pt-5">
                                <div className="col-5">
                                    <div className="d-block">
                                        КОПИЙ
                                    </div>
                                    <TextField
                                        style={{width: 200}}
                                        className="pt-4"
                                        onChange={(e) => {
                                            orderStore.changeField("count", e.target.value)
                                        }}
                                        value={orderStore.order.count}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="d-block float-right pt-4">
                                <div>
                                    КАДРИРОВАНИЕ
                                </div>
                                <div className="pt-4">
                                    <div className="d-inline-block">
                                        <div className="pb-3">
                                            по ширине
                                        </div>
                                        <div
                                            onClick={(e) => {
                                                orderStore.changeField("image", OrderImage.BY_WIDTH)
                                            }}
                                            className={`d-block bg-light select-box ${
                                                orderStore.order.image === OrderImage.BY_WIDTH ? 'orange-border' : null
                                            }`}>
                                        </div>
                                    </div>
                                    <div className="d-inline-block ml-5">
                                        <div className="pb-3">
                                            на все фото
                                        </div>
                                        <div
                                            onClick={(e) => {
                                                orderStore.changeField("image", OrderImage.FULL)
                                            }}
                                            className={`d-block bg-light select-box ${
                                                orderStore.order.image === OrderImage.FULL ? 'orange-border' : null
                                            }`}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {orderStore.order.type === OrderEnumType.INSTAGRAM ? (
                        <div>
                            <div className="row">
                                <div className="col">
                                    <div className="d-block float-left pt-4">
                                        <div>
                                            ШАБОЛОН
                                        </div>
                                        <div className="pt-4">
                                            <div className="d-inline-block  ">
                                                <div className="pb-3">
                                                    обычный
                                                </div>
                                                <div
                                                    onClick={(e) => {
                                                        orderStore.changeField("template", OrderTemplate.MEDIA)
                                                    }}
                                                    className={`d-block bg-light select-box ${
                                                        orderStore.order.template === OrderTemplate.MEDIA ? 'orange-border' : null
                                                    }`}>
                                                </div>
                                            </div>
                                            <div className="d-inline-block ml-5">
                                                <div className="pb-3">
                                                    instagram
                                                </div>
                                                <div
                                                    onClick={(e) => {
                                                        orderStore.changeField("template", OrderTemplate.INSTAGRAM)
                                                    }}
                                                    className={`d-block bg-light select-box ${
                                                        orderStore.order.template === OrderTemplate.INSTAGRAM ? 'orange-border' : null
                                                    }`}>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>) : null}
                </div>
            </Page>
        </div>
    );
});