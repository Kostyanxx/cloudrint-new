import {observer} from "mobx-react";
import React from "react";
import {Page} from "../../common/Page";
import {AppContext} from "../../../stores/Stores";
import ArrowBack from '@material-ui/icons/ArrowBack';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import history from "../../../utils/history";
import {
    OrderTemplate,
    OrderImage,
    OrderEnumType
} from '../../../enums/OrderEnum'
import Uploader from "../../common/Uploader";
import Button from "@material-ui/core/Button";
import Footer from "../../common/Footer";

interface Props {
    order: number
}

export const MobileSettings = observer((props: Props) => {
    const {orderStore, photoStore, pageStore} = React.useContext(AppContext);
    const orderId: number = props.order;

    return (
        <div>
            <Page width={290}>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <ArrowBack
                                className="float-left"
                                onClick={() => pageStore.goBack()}/>
                            <div className="float-left ml-3">
                                ЗАКАЗ ДЛЯ <b style={{color: '#efb476'}}> ПЕЧАТИ </b>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-4">
                            <div className="row pt-4">
                                <div className="col">
                                    <div className="d-block">
                                        РАЗМЕР
                                    </div>
                                    <Select
                                        style={{padding: 5, width: 250}}
                                        className="pt-4"
                                        onChange={(e) => {
                                            orderStore.changeField("size", e.target.value)
                                        }}
                                        value={orderStore.order.size}>
                                        <MenuItem value={1}>10X15 (8 руб/шт)</MenuItem>
                                        <MenuItem value={2}>10X10 (8 руб/шт)</MenuItem>
                                    </Select>
                                </div>
                            </div>
                            <div className="row pt-5">
                                <div className="col-5">
                                    <div className="d-block">
                                        КОПИЙ
                                    </div>
                                    <TextField
                                        style={{width: 250}}
                                        className="pt-4"
                                        onChange={(e) => {
                                            orderStore.changeField("count", e.target.value)
                                        }}
                                        value={orderStore.order.count}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col mt-4">
                            <div className="d-block pt-4">
                                <div>
                                    КАДРИРОВАНИЕ
                                </div>
                                <div className="pt-4">
                                    <div className="d-inline-block">
                                        <div className="pb-3">
                                            по ширине
                                        </div>
                                        <div
                                            onClick={(e) => {
                                                orderStore.changeField("image", OrderImage.BY_WIDTH)
                                            }}
                                            className={`d-block bg-light select-box-mobile ${
                                                orderStore.order.image === OrderImage.BY_WIDTH ? 'orange-border' : null
                                            }`}>
                                        </div>
                                    </div>
                                    <div className="d-inline-block mt-3">
                                        <div className="pb-3">
                                            на все фото
                                        </div>
                                        <div
                                            onClick={(e) => {
                                                orderStore.changeField("image", OrderImage.FULL)
                                            }}
                                            className={`d-block bg-light select-box-mobile ml-3 ${
                                                orderStore.order.image === OrderImage.FULL ? 'orange-border' : null
                                            }`}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {orderStore.order.type === OrderEnumType.INSTAGRAM ? (
                        <div>
                            <div className="row mt-4">
                                <div className="col">
                                    <div className="d-block  pt-4">
                                        <div>
                                            ШАБОЛОН
                                        </div>
                                        <div className="pt-4">
                                            <div className="d-inline-block  ">
                                                <div className="pb-3">
                                                    обычный
                                                </div>
                                                <div
                                                    onClick={(e) => {
                                                        orderStore.changeField("template", OrderTemplate.MEDIA)
                                                    }}
                                                    className={`d-block bg-light select-box-mobile ${
                                                        orderStore.order.template === OrderTemplate.MEDIA ? 'orange-border' : null
                                                    }`}>
                                                </div>
                                            </div>
                                            <div className="d-inline-block mt-3">
                                                <div className="pb-3">
                                                    instagram
                                                </div>
                                                <div
                                                    onClick={(e) => {
                                                        orderStore.changeField("template", OrderTemplate.INSTAGRAM)
                                                    }}
                                                    className={`d-block bg-light select-box-mobile ml-3 ${
                                                        orderStore.order.template === OrderTemplate.INSTAGRAM ? 'orange-border' : null
                                                    }`}>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>) : null}
                        <div className="row mt-5">
                            {orderStore.order.type === OrderEnumType.INSTAGRAM
                                && !orderStore.order.hasPhotos && (
                                <div className="col">
                                    <Button className="p-3 float-right"
                                            onClick={() => {
                                                 localStorage.setItem('order_id', orderId.toString());
                                                    window.location.href = 'https://www.instagram.com/oauth/authorize/?' +
                                                    'client_id=1be32d2bb9124e87a0402693adaf149e' +
                                                    '&redirect_uri=http://localhost:3000/instagram&response_type=token'
                                            }}
                                            style={{border: 'solid 2px #f2c492', width: '100%'}}>
                                        ВЫБРАТЬ ФОТО
                                    </Button>
                                </div>
                            )}
                            {(orderStore.order.type === OrderEnumType.DEFAULT
                                && !orderStore.order.hasPhotos) && (
                                <div className="col">
                                    <Uploader
                                        sizes={{width: 270, height: 60}}
                                        url={`http://localhost:8000/api/clients/orders/${orderId}/photo`}
                                        beforeSelect={() => {
                                            orderStore.saveOrder();
                                            history.push('/order/' + orderId + '/photos');
                                        }}
                                        fields={[{
                                            name: "order_id", value: orderId
                                        }]}/>
                                    <Button className="p-3 float-right"
                                            style={{border: 'solid 2px #f2c492', width: '100%'}}>
                                        ВЫБРАТЬ ФОТО
                                    </Button>
                                </div>
                            )}
                            {orderStore.order.hasPhotos && (
                                <div className="col">
                                    <Button className="p-3 float-right"
                                            onClick={() => {
                                                 orderStore.saveOrder();
                                                 photoStore.reset();
                                                 history.push('/order/' + orderId + '/photos');
                                            }}
                                            style={{border: 'solid 2px #f2c492', width: '100%'}}>
                                        СОХРАНИТЬ
                                    </Button>
                                </div>
                            )}
                        </div>
                    <Footer/>
                </div>
            </Page>
        </div>
    );
});