import React from 'react';
import { HomeComponent } from "./components/pages/home/HomePage";
import { InstagramPage } from "./components/pages/instagram/InstagramPage";
import { OrderPage } from "./components/pages/order/OrderPage";
import { PhotoPage } from "./components/pages/photo/PhotoPage";
import { SettingsPage } from "./components/pages/settings/SettingsPage";
import { PhotoEditPage } from "./components/pages/photo-edit/PhotoEditPage";
import { InfoPage } from "./components/pages/info/InfoPage";
import { Login } from "./components/pages/admin/Login";
import { Orders } from "./components/pages/admin/Orders";
import Error from "./components/common/Error";
import { Route, Switch, Redirect } from 'react-router';
import { Router } from 'react-router-dom';
import { Provider } from 'mobx-react';
import history from './utils/history';

const App: React.FC = () => {
  return (
      <div>
          <Provider>
              <Router history={ history }>
                  <Switch>
                      <Route exact path="/order/:id" component={ SettingsPage }/>
                      <Route exact path="/order/:id/photos" component={ PhotoPage }/>
                      <Route exact path="/orders" component={ OrderPage }/>
                      <Route exact path="/instagram" component={ InstagramPage }/>
                      <Route exact path="/photo/edit" component={ PhotoEditPage }/>
                      <Route exact path="/info" component={ InfoPage }/>
                      <Route exact path="/admin" component={ Login }/>
                      <Route exact path="/admin/orders" component={ Orders }/>
                      <Route exact path="/" component={ HomeComponent }/>
                      <Route path="/" component={ Error }/>
                  </Switch>
              </Router>
          </Provider>
      </div>
  );
};

export default App;
