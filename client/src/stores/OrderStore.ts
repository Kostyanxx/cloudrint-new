import {observable, action, toJS} from 'mobx';
import {orderService} from '../services/OrderService'
import {OrderType} from '../types/OrderType'
import {
    OrderImage,
    OrderTemplate,
    OrderSize,
    OrderEnumType
} from '../enums/OrderEnum'
import history from "../utils/history";
import pageStore from "./PageStore";

class OrderStore {
    @observable order: OrderType = <OrderType>{};

    constructor() {
        this.order.count = 1;
        this.order.type = 1;
        this.order.image = OrderImage.BY_WIDTH;
        this.order.size = OrderSize.SIZE_10X15;
        this.order.template = OrderTemplate.MEDIA;
    }

    @action
    changeField = (field: string, value: any) => {
        this.order[field] = value;
    };

    @action
    getOrder = async (id: number) => {
        pageStore.startLoading();
        const order = await orderService.getOrder(id);
        if (order['id']) {
            this.order = order;
        }
        pageStore.endLoading();
    };

    @action
    createOrder = async (type: OrderEnumType) => {
        pageStore.startLoading();
        this.order.type = type;
        const response = await orderService.createOrder(this.order);
        if (response['order_id']) {
            history.push(`/order/${response['order_id']}`);
        }
        pageStore.endLoading();

    };

    @action
    saveOrder = async () => {
        pageStore.startLoading();
        await orderService.saveOrder(this.order.id, this.order);
        pageStore.endLoading();
    };
}

export default new OrderStore();