import {action, observable, toJS} from 'mobx';
import {photoService} from '../services/PhotoService'
import {Photos, PhotoType} from '../types/PhotoType'
import history from "../utils/history";
import {getContentImageByUrl, getImage} from "../utils/image";
import {InstagramGalleryPhotoType} from "../types/InstagramGalleryPhotoType";
import {InstagramPhotoType} from "../types/InstagramPhotoType";
import pageStore from "./PageStore";
import orderStore from "./OrderStore";

class PhotoStore {
    @observable photos: Photos = [];
    @observable count: number = 0;
    @observable page: number = 1;
    @observable order: number = 0;
    @observable hasNext: boolean = false;
    @observable editedPhoto: PhotoType = <PhotoType>{};
    @observable instagramGalleryPhotos: InstagramGalleryPhotoType[] = [];
    @observable instagramPhotos: InstagramPhotoType[] = [];
    @observable instagramNextUrl?: string | null = null;
    @observable instagramAllSelected: boolean = false;
    @observable instagramSelectedCount: number = 0;
    @observable instagramLoadModal: boolean = false;
    @observable instagramUploadedPhotos: number = 0;
    @observable instagramLoadingPhotos: number = 0;
    @observable instagramUserPhoto?: string;

    constructor() {}

    @action
    reset = () => {
        this.photos = [];
        this.count = 0;
        this.page = 1;
        this.hasNext = false;
        this.editedPhoto = <PhotoType>{};
        this.getPhotos(this.order);
    };

    @action
    nextPage = () => {
        this.page = this.page + 1;
        this.getPhotos(this.order);
    };

    @action
    goToSettings = () => {
        history.push('/order/' + this.order);
    };

    @action
    setEditedPhoto = (photo: PhotoType) => {
        this.editedPhoto = {...photo};
    };

    @action
    savePhoto = async () => {
        pageStore.startLoading();
        try {
            const editedPhoto = {...this.editedPhoto, content: null, preview: null};
            const response = await photoService.savePhoto(editedPhoto);
            if (response && response['photo_id']) {
                getImage(this.editedPhoto, orderStore.order, this.instagramUserPhoto).then((photo: PhotoType) => {
                    let photoS: PhotoType = <PhotoType>this.photos.find(
                        (row) => row.id === this.editedPhoto["id"]
                    );
                    photoS["preview"] = photo["preview"];
                    photoS["scale"] = photo["scale"];
                });
                history.goBack();
            }
            pageStore.endLoading();
        } catch(e) {
            pageStore.endLoading();
        }
    };

    @action
    deletePhoto = async () => {
        pageStore.startLoading();
        const response = await photoService.deletePhoto(this.editedPhoto.id);
        if (response && response['success']) {
            let photos = [];
            for (let photo of this.photos) {
                if (photo["id"] !== this.editedPhoto.id) {
                    photos.push(photo);
                }
            }
            this.photos = photos;
            this.count = this.count - 1;
            history.goBack();
        }
        pageStore.endLoading();
    };

    @action
    getPhotos = async (orderId: number) => {
        pageStore.startLoading();
        const response = await photoService.getPhotos(orderId, this.page);

        if (orderStore.order.template == 2 && !this.instagramUserPhoto) {
            const userProfileImageResp = await photoService.getInstagramUserPhoto(orderId);

            if (userProfileImageResp.content) {
                this.instagramUserPhoto = 'data:image/jpeg;base64,' + userProfileImageResp.content;
            }
        }

        if (response && response['photos'].length > 0) {
            let photoLoad = response['photos'];
            let photoPromises = [];
            for (let i in photoLoad) {
                let photo = photoLoad[i];
                photo["index"] = i;
                photo.content = 'data:image/jpeg;base64,' + photo.content;
                if (photo.scale) {
                    photo.scale = parseFloat(photo.scale);
                } else {
                    photo.scale = 1;
                }
                photoPromises.push(getImage(
                    photo, orderStore.order, this.instagramUserPhoto
                ));
            }
            Promise.all(photoPromises).then((photoEdited: any) => {
                this.photos = [...this.photos,...photoEdited];
            });
            this.count = response['count'];
            this.order = orderId;
            this.hasNext = response['has_next']
        }
        pageStore.endLoading();
    };

    @action
    getInstagramPhotos = async (token?: string | false, loadMore?: boolean) => {
        let response: any;
        let galleryPhotos = [];
        let photos = [];

        pageStore.startLoading();
        if (this.instagramNextUrl && loadMore) {
            response = await photoService.sendRequest(
                this.instagramNextUrl, 'GET', false
            );
        } else {
            if (token)
                response = await photoService.getInstagramPhotos(token);
        }

        if (response['data']) {
            const responseData = response['data'];
            pageStore.startLoading();
            for (let i in responseData) {
                const responseDataItem = responseData[i];
                const imgData = responseDataItem.images["low_resolution"];

                if (
                    responseDataItem["type"] === "image"
                    || responseDataItem["type"] === "carousel"
                ) {
                    const previewSource =  await getContentImageByUrl(imgData["url"], imgData["width"], imgData["height"]);

                    galleryPhotos.push({
                        id: responseDataItem["id"],
                        src: 'data:image/jpeg;base64,' + previewSource,
                        thumbnail: 'data:image/jpeg;base64,' + previewSource,
                        thumbnailWidth: imgData["width"],
                        thumbnailHeight: imgData["height"],
                        isSelected:  this.instagramAllSelected,
                    });

                    photos.push({
                        id: responseDataItem["id"],
                        location: responseDataItem["location"],
                        username: responseDataItem.user.username,
                        user_picture: responseDataItem.user.profile_picture,
                        likes: responseDataItem.likes.count,
                        url: responseDataItem.images["standard_resolution"].url,
                        width: responseDataItem.images["standard_resolution"].width,
                        height: responseDataItem.images["standard_resolution"].height,
                        date: responseDataItem["created_time"]
                    })
                }
            }
            if (this.instagramAllSelected) {
                this.instagramSelectedCount = this.instagramSelectedCount + galleryPhotos.length;
            }
            this.instagramPhotos = [...this.instagramPhotos,...photos];
            this.instagramGalleryPhotos = [...this.instagramGalleryPhotos,...galleryPhotos];
        }

        if (response['pagination'].next_url) {
            this.instagramNextUrl = response['pagination'].next_url;
        } else {
            this.instagramNextUrl = null;
        }
        pageStore.endLoading();
    };

    @action
    selectInstagramPhoto = (index: number) => {
        const img = this.instagramGalleryPhotos[index];
        let photo: InstagramGalleryPhotoType =
            <InstagramGalleryPhotoType>this.instagramGalleryPhotos.find(
                (row) => row.id === img.id
            );
        photo.isSelected = !photo.isSelected;

        if (photo.isSelected) {
            this.instagramSelectedCount++;
        } else {
            this.instagramSelectedCount--;
        }
    };

    @action
    toggleAllInstagramPhoto = () => {
        this.instagramAllSelected = !this.instagramAllSelected;

        if (this.instagramAllSelected) {
            this.instagramSelectedCount = this.instagramGalleryPhotos.length;
            for (let i in this.instagramGalleryPhotos) {
                this.instagramGalleryPhotos[i].isSelected = true;
            }
        } else {
            this.instagramSelectedCount = 0;
            for (let i in this.instagramGalleryPhotos) {
                this.instagramGalleryPhotos[i].isSelected = false;
            }
        }
    };

    @action
    instagramLoadPhotos = async (token?: string) => {
        this.instagramLoadModal = true;
        this.instagramLoadingPhotos = this.instagramSelectedCount;
        this.instagramUploadedPhotos = 0;

        const orderId: number = parseInt(<string>localStorage.getItem('order_id'));
        await orderStore.getOrder(orderId);

        const photos: InstagramGalleryPhotoType[] =
            this.instagramGalleryPhotos.filter(
                photo => photo.isSelected
            );

        if (!orderStore.order.hasPhotos) {
            const response = await photoService.sendRequest(
                'https://api.instagram.com/v1/users/self',
                'GET', false, {
                    access_token: token
                }
            );
            if (response.data.profile_picture) {
                const instagramUserPhoto = await getContentImageByUrl(
                    response.data.profile_picture,
                    150,
                    150
                );
                await photoService.saveInstagramUserPhoto(
                    orderId, instagramUserPhoto
                );
            }
        }

        for (let i in this.instagramPhotos) {
            const photo = {...this.instagramPhotos[i]};
            for (let j in photos) {
                if (photos[j].id === photo.id) {
                    photo.content = await getContentImageByUrl(
                        photo.url,
                        photo.width,
                        photo.height
                    );
                    const response = await photoService.saveInstagramPhoto(
                        orderId, photo
                    );
                    if (response['photo_id']) {
                        this.instagramUploadedPhotos++;
                        this.instagramSelectedCount--;
                        photos[j].isSelected = false;
                    }
                }
                if (this.instagramUploadedPhotos === this.instagramLoadingPhotos) {
                    this.instagramLoadModal = false;
                    history.push('/order/'+orderId+'/photos');

                    return false;
                }
            }
        }
    };

    @action
    instagramCloseUploadModal = () => {
        this.instagramLoadModal = false;
    };
}

export default new PhotoStore();