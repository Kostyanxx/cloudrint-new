import {observable, action, toJS} from 'mobx';
import {authService} from "../services/AuthService";
import history from "../utils/history";

class PageStore {
    @observable isLoading: boolean = false;
    @observable deviceVersion: string = 'desktop';
    @observable deviceWidth: number = 700;

    constructor() {
        const token = localStorage.getItem('token');
        if (!token) {
            this.authClient();
        } else {
            this.checkToken();
        }
    }

    async authClient() {
        this.startLoading();
        const response: any = await authService.createToken();
        if (response['token']) {
            localStorage.setItem('token', response['token']);
        }
        this.endLoading();
    }

    async checkToken() {
        this.startLoading();
        const response: any = await authService.checkToken();
        if (!response['success']) {
            this.authClient();
        }
        this.endLoading();
    }

    @action
    startLoading = () => {
        this.isLoading = true;
    };

    @action
    endLoading = () => {
        this.isLoading = false;
    };

    @action
    goBack = () => {
        history.goBack();
    }
}

export default new PageStore();