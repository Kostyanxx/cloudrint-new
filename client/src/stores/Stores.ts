import * as React from "react";
import orderStore from './OrderStore';
import photoStore from './PhotoStore';
import pageStore from './PageStore';

export function createStores() {
    return {
        pageStore,
        orderStore,
        photoStore,
    };
}

export const stores = createStores();

export const AppContext = React.createContext(stores);